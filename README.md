<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Workshop Materials

This repository contains the materials contributed by HIFIS to [the HiPerCH 12 workshop, module 2: "Introduction to Research Software Engineering"](https://www.hkhlr.de/en/articles/hiperch-12-module-2).

## Session 1: Introduction to Software Processes and Change Management

### Live-Demo: "Software project management: Branching, working on features and merge back“

The demonstration is based on the [Workshop: Software Development for Teams](https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/workshop-materials/-/blob/1.0.0/03_example-project/README.md):

- [These slides](01-demo-add-a-new-feature/slides.pptx) explain further details about the project setup.
- [These notes](01-demo-add-a-new-feature/demo-workflow.md) summarize the demonstration workflow.
- We use [this GitLab project](https://gitlab.com/hifis/hifis-workshops/hiperch-12/sample-calculator-demo-workflow) to demonstrate the workflow. It is based on the [Sample Calculator - 03a-prepare-iteration-3](https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/sample-calculator/-/tree/03a-prepare-iteration-3).

## Session 2: Software Engineering in Support for Sustainable Science

### Live-Demo: "Making a script reuse- and publishable"

The demonstration is based on the [Workshop: Bring Your Own Script and Make It Ready for Publication](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/1.0.1/README.md):

- [These slides](02-demo-make-a-script-reuse-and-publishable/slides.pptx) explains the step neccessary to make a script reuse- and publishable. The slides are slightly modified versions of the slides of the [
Bring Your Own Script and Make It Ready for Publication](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/tree/master/slides) training.
- Live-Demo: The code shown during the presentation can be found [here](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis). Each step is represented by a separate branch.

### Presentation: "Beyond scripting - Sustainable Software Engineering for Software Projects"

[These slides](02-talk-beyond-scripting-sustainable-se-for-software-products/slides.pptx) are the basis for an open presentation about further steps you need to take if your software grows.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
