<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Demonstration: Add the Average Calculation

In the following, we explain how to add the missing calculations.
You can find the prepared source code in the [code](code) subdirectory.

## Preparation

- Create a corresponding issue using the `Change` template and add all relevant information (title, description, assignee, label, milestone).
- Create a merge request from the issue and edit it:
  - Use the `Review` template
  - Set the issue ID in the merge request description (e.g., `Fix #1`)
- Clone the Git repository and switch to the new branch.

## Add the Average Calculation and its Tests

- Copy the calculation `<Calculation Name>_calculation.py` into the directory `sample_calculator/calculations/calculations`.
- Copy the corresponding test `<Calculation Name>_calculation_test.py` into the directory `tests/calculations/calculations`.

## Integrate the new calculation

- Edit the file `sample_calculator/calculations/__init__.py` and add the corresponding import statement.
  The final list might look as follows:

```
from sample_calculator.calculations.calculations.average_calculation import AVERAGE_CALCULATION
```

- Edit the file `sample_calculator/main.py` and add the calculation constant to the list of calculations in the function `_perform_calculation`.
  The final list might look as follows:

```
def _perform_calculation(sample_values):
    return_code = _SUCCESS
    calculations_ = calculations.create_calculation([calculations.AVERAGE_CALCULATION])
```

> Please make sure that the `SUM_CALCULATION` is not part of the list.
> It should not be printed as part of the results.

## Integrate your Changes

### Commit and Push your Changes

- Commit locally and use a meaningful commit message.
- Push your changes.
- Make sure that the build pipeline ran successfully.

### Review the Changes

- A member of your team should perform the review.
- If everything is fine, approve the merge request.
  Otherwise correct the findings.

> The merge request cannot be approved by the merge request author in this example.

### Merge the Changes

- Resolve the `Draft` status and merge the changes.
- Make sure that:
  - The following build pipeline ran successfully.
  - Your feature branch has been deleted.
  - The issue has been closed.
